from flask import Flask, render_template
import requests
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text
import os
import dotenv

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = f"postgresql://{os.getenv('POSTGRES_USER')}:{os.getenv('POSTGRES_PASSWORD')}@{os.getenv('POSTGRES_HOST')}:{os.getenv('POSTGRES_PORT')}/{os.getenv('POSTGRES_DB')}"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy( app )

class CityTable( db.Model ):
    __tablename__ = "city"
    id = db.Column( db.Integer, primary_key=True )
    city = db.Column( db.String(50) )
    lat = db.Column( db.String(20) )
    lon = db.Column( db.String(20) )
    def __init__( self, city, lat, lon ):
        self.city = city
        self.lat = lat
        self.lon = lon
    def __str__(self) -> str:
        return f"{self.id}"
db.create_all() 

@app.route("/")
def home():
    return render_template("index.html")
@app.route("/about")
def about():
    return render_template("about.html", title = "About")

def get_coords(city: str) -> dict:
    coords = {}
    response = requests.get("https://nominatim.openstreetmap.org/search", params={"format":"json", "city":city})
    if response.status_code == 200:
        answer = response.json()
        if len(answer) > 0:
            coords = {"city": city, "lat": answer[0]["lat"], "lon": answer[0]["lon"]}
    return coords

def savecity( db, city ):
    t = text("select * from city where city=:city" )
    geo = db.engine.execute( t, city=city ).fetchall()
    if len(geo) != 0:
        print( geo[0])
        for g in geo:
            coords = {"city": g[1], "lat": g[2], "lon": g[3]}
            return coords
    coords = get_coords( city )
    if coords: 
        dat = CityTable( city, coords['lat'], coords['lon'] )
        db.session.add(dat)
        db.session.commit()
    return coords

@app.route("/cities/<city>")
def showcity(city):
    if city:
        d = savecity( db, city )
    return render_template( "city.html", city=city, lat=d['lat'], lon=d['lon'])


if __name__ == "__main__":
    app.run()